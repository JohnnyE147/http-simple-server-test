#!/bin/sh

#  unitTest.sh
#  Test
#
#  Run unit tests
#  should be run from CProjects/Test. For example:
#   Scripts/unitTest.sh
#
#  Created by Johnny on 1/8/18.
#  Copyright © 2018 JonathonElderton. All rights reserved.

../Build/Test 8080 "/Users/Johnny/testdir" &

# use curl to test get and post
echo "Unit: Getting directory contents"
curl http://localhost:8080/

echo "Unit: Sending text file"
curl --data-ascii 'abcdefhijklmnopqrstuvwxyz' http://localhost:8080/test.txt

echo "Unit: Getting new text file"
curl http://localhost:8080/test1.txt

# kill the program
pkill Test
