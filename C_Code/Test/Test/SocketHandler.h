//
//  SocketHandler.h
//  Test
//
//  Header file for SocketHanlder. Contains all public fuctions. 
//
//  Created by Johnny on 1/6/18.
//  Copyright © 2018 JonathonElderton. All rights reserved.
//

#ifndef SocketHandler_h
#define SocketHandler_h

// public functions decloration
void startConnection (int portNum, const char *dir);

#endif /* SocketHandler_h */
