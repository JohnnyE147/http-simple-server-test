//
//  main.c
//  Test
//
//  Starts a simple http local host server connection, using a
//  server socket. It needs a port number and a root direcotry to access.
//
//  Created by Johnny on 1/6/18.
//  Copyright © 2018 JonathonElderton. All rights reserved.
//

#include "SocketHandler.h"

#include <stdio.h>
#include <string.h>

static const int DEFAULT_PORT_NUM = 8080;
static const char DEFAULT_TOP_DIR[22] = "/Users/Johnny/testdir";

/**
 *  Main can take in inputs from the client when creating a server socket.
 *  It can be at a port that client can provide. Cleint also can supply a
 *  root directory. If the user does not pase in a root directory a defualt
 *  directory is used. If client does not pass in a port number then they cannot
 *  pass in a root directory.
 *
 *  User passes port number and then root directory. For example:
 *      ./Test 8080 "/Users/Johnny/testdir"
 *
 **/
int main(int argc, const char * argv[]) {

    // initialization
    // if users does not pass in inputs
    int portNum = DEFAULT_PORT_NUM;
    char topDir[100];
    strcpy(topDir, DEFAULT_TOP_DIR);
    
    /* parse input arg */
    if (argc >= 1) {
        // user supplies the port number
        if (argv[1] != NULL){
            portNum = atoi(argv[1]);
        }
    }
    if (argc >= 2) {
        if (argv[2] != NULL){
            // user supplies the root directory
            strcpy(topDir, argv[2]);
        }
    }
    
    // displays user input
    //printf("Listening on port %d in directory %s\n", portNum, topDir);
    // starts the server connection
    startConnection(portNum, topDir);
    
    return 0;
}
