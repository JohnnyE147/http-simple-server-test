//
//  SocketHandler.c
//  Test
//
//  Creates a socket at port given from client or using default.
//  Then ensures connection, takes the input from client, parses
//  the input stream, then handles the request accordingly.
//
//  User can request:
//      GET all files from directory
//      GET data from file in directory
//      POST file with data into directory
//
//  Created by Johnny on 1/6/18.
//  Copyright © 2018 JonathonElderton. All rights reserved.
//

#include "SocketHandler.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <dirent.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <string.h>

#define CHUNKSIZE 1024 // 1 K

/*
    object content contains char array
    of first header of request from client,
    char array of the data from the request
    (only needed for POST), and contains length
    of data.
 */
typedef struct _content {
    char header[100];       // first header
    char *data;
    int dataLength;
} content;

static char topDir[100];

// private functions declaration
static content *parseRequest (char *buffer, int len);
static void processRequest (content *object, char *fileName, int serverSocket);
static void handleGetRequest (int serverSocket);
static void handleGetFileRequest (char *fileName, int serverSocket);
static void handlePostRequest (content *object, char *fileName);
static char *parseFileName (char *buffer);

/**
 *  Starts connection to http local host server, using sockets.
 *  It requires 5 steps to connect to server at port number, read
 *  request from client then access a directory at topDir to complete
 *  clients request. Client can GET text information of director, GET
 *  data from a file in directory, and POST file to directory.
 *
 *  Steps:
 *      1. creating 1st socket and connecting socket to port number
 *      2. binding 1st socket to the address of server and starts the server
 *      3. listens for client request to server and accpets connection using 2nd socket
 *      4. from 2nd socket, starts communicating with client, reads request from client
 *         and parses request
 *      5. once pasrsing is complete, information is either read or writen into directory
 *
 *  Once steps 3 through 5 are complete then the server waits for another
 *  request from the client
 *
 *  portNum:    int, which is the port number in which server is created
 *  *dir:       char array, of the root directory in which request are performed
 **/
void startConnection (int portNum, const char *dir){
    // initialization of sockets and client
    int serverSocket, newServerSocket, client;
    // creates buffer
    char buffer[1024];
    // creates struct obeject
    content *req = NULL;
    // creates socket structure
    struct sockaddr_in serverAddr, clientAddr;
    ssize_t n = 0;
    
    // stores dir into topDir
    strcpy(topDir, dir);
    
    // Step 1
    // create socket
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (serverSocket < 0) {
        printf("ERROR opening socket\n");
        return;
    }
    
    // initializing socket structure
    bzero((char *) &serverAddr, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(portNum);
    
    // Step 2
    // binding the host address
    if (bind(serverSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0) {
        // could not bind address to socket
        printf("ERROR on binding\n");
        return;
    }
    
    // runs http server forever
    while(1) {
        /*
            starts listening for clients, process will go
            into sleep mode and wait for incoming connection
         */
        listen(serverSocket,5);
        client = sizeof(clientAddr);
    
        // Step 3
        // accept actual connection from the client
        newServerSocket = accept(serverSocket, (struct sockaddr *)&clientAddr, &client);
        if (newServerSocket < 0) {
            // could not accept connection, end program
            printf("ERROR on accept\n");
            break;
        }
        
        // Step 4
        // if connection is established then start communicating
        bool done = false;
        // storing request from client
        void *message = malloc(CHUNKSIZE);
        int messageSize = CHUNKSIZE;
        int messageLength = 0;
        
        // reading the message from the client
        // reads request in chunks
        while (done == false){
            // read a chunk of 1024
            // from the socket
            bzero(buffer,CHUNKSIZE);
            n = read(newServerSocket,buffer,CHUNKSIZE);
            
            if (n > 0){
                // copy chunk to message
                int avail = (messageSize - messageLength);
                if (n > avail){
                    // makes more memory if needed
                    messageSize += CHUNKSIZE;
                    message = realloc(message, messageSize);
                }
                // copies buffer into message in chunks
                memcpy(message + messageLength, buffer, n);
                messageLength += n;
            }
            
            if (n != CHUNKSIZE){
                break;
                // nothing more to read
            }
        }
        if (n < 0) {
            // could not read from socket
            // closes socket and restarts the connection
            printf("ERROR reading from socket\n");
            close(newServerSocket);
            continue;
        }
        
        // Step 4
        // parse the message to see request
        /*
            first line of request should look something like
                GET / HTTP/1.1                    -- no arguments so return the whole directory
                GET /image.png HTTP/1.1           -- returns single image.png file
                POST /newImage.png HTTP/1.1       -- create and save newImage.png to directory
         */
        req = parseRequest((char *) message, messageLength);
        char *fileName = NULL;
        
        // making sure header and struct content are not null
        if (req != NULL && req->header[0] != '\0'){
            fileName = parseFileName(req->header);
        } else {
            //printf("ERROR reading request\n");
            free(message);
            continue;
        }
        // cleanup
        free(message);
    
        // Step 5
        // process the request from client
        if (newServerSocket > 0){
            processRequest(req, fileName, newServerSocket);
        }
        // displays the request from client
        printf("Request from client: %s\n",buffer);
        
        // cleanup
        free(req);
        free(fileName);
        close(newServerSocket);
    }
}

/**
 *  Parses the request from client; and returns a object
 *  containing the headers, data, and length of data from
 *  request. Headers and data in request are seperated by
 *  substring "\r\n\r\n". First determains the size of data
 *  from cleint, this is only for POST. If there is data
 *  present then creates the right amount of mem for data char.
 *  Then parses headers from the request and returns object
 *  containing the content.
 *
 *  *buffer:    char array, containing the request from client
 *  len:        int, which is the length of buffer
 *  returns:    struct content, the contents from request
 **/
static content * parseRequest (char *buffer, int len){
    /*
        struct containing the the first header of request,
        the data from request, and the length of data.
     */
    content *request = (content *)malloc(sizeof(content));
    // counter
    int i = 0;
    
    // determain the length of data in buffer
    while (i< len){
        if (buffer[i] == '\r' && buffer[i+1] == '\n'){
            // newline
            i += 2;
            // found the sub string "\r\n\r\n"
            if (buffer[i] == '\r' && buffer[i+1] == '\n'){
                // end of header section
                i += 2;
                // stores the length of data
                request->dataLength = (len - i);
                break;
            }
        } else {
            // increases counter
            i += 1;
        }
    }
        
    // if there is a data section, allocate mem for it
    if (request->dataLength > 0) {
        request->data = malloc((request->dataLength) + 1);
        memcpy(request->data, buffer + i, request->dataLength);
    }
    
    // parse first header
    i = 0;
    while (i < (len - request->dataLength + 4)){
        // looks for new line in buffer, which is "\r\n"
        if (buffer[i] == '\r' && buffer[i+1] == '\n'){
            // once found break
            break;
        }
        else {
            // adds conents of
            request->header[i] = buffer[i];
        }
        
        i++;
    }
    request->header[i] = '\0';

    return request;
}

/*
 *  Parse the name of file from the first header
 *  of client request. By determaining the starting
 *  position file name. It determains if request is
 *  a GET or POST then starts accordingly. It starts
 *  position after " /". Then returns the file name.
 *
 *  *buffer:    char array, conatins the firs header from client request
 *  returns:    parsed string, containing the fileName
 */
static char * parseFileName (char *buffer){
    // string of file name
    char name[100];
    // counters
    int i = 0;
    int currentCount = 0;
    
    /*  determain if first 3 letter are GET
        if not, then determain if first 4 letters
        are POST */
    if (strncmp(buffer, "GET ", 4) == 0) {
        // start at position 6
        // starts after " /"
        i = 5;
    } else if (strncmp(buffer, "POST ", 5) == 0) {
        // start at position 7
        // starts after " /"
        i = 6;
    } else {
        // bails if problem
        printf("ERROR could not parse %s \n", buffer);
        return NULL;
    }
    /*
        once pointer is at correct position
        the file name can then be parsed to
        the ' ' char
     */
    while (buffer[i] != '\0'){
        if (buffer[i] != ' '){
            // increase counters
            name[currentCount] = buffer[i];
            i += 1;
            currentCount += 1;
        } else {
            // once ' ' is hit, break
            break;
        }
    }
    // adds null char to end of fileName
    name[currentCount] = '\0';
    // mallocates memory for the new string
    return strdup(name);
}

/**
 *  Process the request from client.
 *  Client can GET and POST, which depends on
 *  request. It looks at headers and filename, parsed
 *  from the request to find what client requested.
 *
 *  *object:        struct content, containing headers and data from request
 *  *fileName:      char array, containing name of file to be found
 *  serverSocket:   int, which is the socket to http server connection
 **/
static void processRequest (content *object, char *fileName, int serverSocket){
    /*
        looks at first char in header to see if
        request is a GET or POST
     */
    if (object->header[0] == 'G'){
        if (fileName == NULL || fileName[0] == '\0'){
            // handles GET request to display directory
            handleGetRequest(serverSocket);
            // then ends
            return;
        } else {
            // handles GET requst to find file
            handleGetFileRequest(fileName, serverSocket);
            // then ends
            return;
        }
        
    } else if (object->header[0] == 'P' && fileName[0] != '\0'){
        // handles post request
        handlePostRequest(object, fileName);
        // then ends
        return;
    } else {
        printf("ERROR reading request from client.\n");
        return;
    }
}

/**
 *  Handles GET request, to see all contents in directory.
 *  It opens 'topDir' and loops through all of contents
 *  then displays each item in its own line back to the client.
 *
 *  serverSocket:   int, which is the socket to http server connection
 **/
static void handleGetRequest (int serverSocket) {
    // code for writing to socket
    ssize_t n = 0;
    // points to content in topDir
    struct dirent *entry;
    // directory pointer
    DIR *root;

    // making sure topDir exists
    if ((root = opendir(topDir)) == NULL){
        printf("ERROR directory %s is not found\n" ,topDir);
    } else {
        // while there is still content to find
        while ((entry = readdir(root)) != NULL){
            // not displaying binary files
            if (entry -> d_name[0] != '.'){
                // Write a response to the client
                n = write(serverSocket,entry -> d_name, strlen(entry -> d_name));
                // if bad write bail
                if( n < 0){
                    printf("ERROR writing to socket\n");
                    break;
                }
                n = write(serverSocket,"\n", 1);
                // if bad write bail
                if( n < 0){
                    printf("ERROR writing to socket\n");
                    break;
                }
            }
        }
    }
    
    // clean up drectory
    if (root != NULL){
        closedir(root);
    }
}

/**
 *  Handles GET request of file in 'topDir'.
 *  By creating a path in topdir with fileName,
 *  then checking if file is present in topDir and
 *  can be read. If both are true then reads the data
 *  in the file, then writes the data back to the client
 *  using the socket.
 *
 *  *fileName:      char array, containing name of file to be found
 *  serverSocket:   int, which is the socket to http server connection
 **/
static void handleGetFileRequest (char *fileName, int serverSocket){
    // the path to file 'fileName'
    char path[CHUNKSIZE];
    // file pointer
    FILE *fp = NULL;
    // creating the path to file
    strcpy(path, topDir);
    strcat(path, "/");
    strcat(path, fileName);
    // opens file to read
    fp = fopen(path, "r");
    
    // if file can not be found or read, then bail
    if (fp == NULL){
        printf("ERROR unable to read file %s \n", path);
        return;
    }
    
    // array to store data from file
    char buffer[CHUNKSIZE];
    // the amount of data from file
    size_t amount = 0;
    
    // reads the file in chunks
    while (1) {
        amount = fread(buffer, sizeof(char), CHUNKSIZE, fp);
        // displays the contents of file to client
        if (amount > 0){
            write(serverSocket, buffer, amount);
        } else {
            break;
        }
    }
    
    // clean up
    fclose(fp);
    
}

/**
 *  Handles a POST request, by creating the new file
 *  with the name 'fileName'. Creates path directly
 *  into 'topDir'. Then writes data into the new
 *  created file.
 *
 *  *object:    struct content, containing data from request
 *  *fileName:  char array, containing name of new file to be created
 **/
static void handlePostRequest (content *object, char *fileName){
    
    // initializing path name
    char path[CHUNKSIZE];
    // file pointer
    FILE *fp = NULL;
    // create the new file path
    strcpy(path, topDir);
    strcat(path, "/");
    strcat(path, fileName);
    // create the file in topDir
    // and opens file to write
    fp = fopen(path, "w");
    
    /*  if file is unable to be created or written into
        end and display error */
    if (fp == NULL){
        printf("ERROR unable to write to file %s \n", path);
        return;
    }
    
    // writes data into new file created
    fwrite(object ->data, sizeof(char), object ->dataLength, fp);
    
    // clean up
    fflush(fp);
    fclose(fp);
    
}



