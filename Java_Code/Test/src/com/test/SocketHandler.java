package com.test;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *  SocketHandler
 *
 *  Controls the socket which connects the client to the server.
 *  The client can request the following:
 *
 *  A GET request will return back a text-based list of a directory
 *  A GET request will return back a file from a directory
 *  A POST request will place a file into a directory
 *
 *  It opens the server connection forever and waits for another
 *  request from the client.
 */
public class SocketHandler {

    // storing root directory
    private File topDir;

    /**
     *  Starts server by creating a server socket, waits for client to connect,
     *  the creates input and output streams. Afterwords it reads the request
     *  from input stream. Parses request and depending on request client can:
     *          GET the whole directory
     *          GET a file from the directory and see all its data
     *          POST a file to the directory
     *  After a request is completed, the server waits for another request.
     *
     *  @param portNumber       The port number for local server
     *  @param dir              The root directory
     *  @throws IOException     Due to socket may not connect
     */
    public void start(int portNumber, File dir) throws IOException{
        // logging
        Logger.debug("Creating a server socket at port " + portNumber);
        // saving portNum and top directory
        ServerSocket serverSocket = new ServerSocket(portNumber);
        topDir = dir;

        // server runs forever, until shut down
        while (true){

            // wait for client to connect
            Socket socket = serverSocket.accept();
            // checks if socket is viable
            if (socket != null) {
                // create input and output streams
                InputStream in = null;
                OutputStream out = null;
                try {

                    // read the request
                    in = socket.getInputStream();
                    Content request = readRequest(in);

                    // empty request?
                    if (request == null || request.headers.size() == 0){
                        // restart
                        socket.close();
                        Logger.error("ERROR Request is empty\n Restarting socket at port " + portNumber);
                        continue;
                    }

                    // examine first line of request
                    // should look something like
                    //    GET / HTTP/1.1                    -- no arguments so return the whole directory
                    //    GET /image.png HTTP/1.1           -- returns single image.png file
                    //    POST /newImage.png HTTP/1.1       -- create and save newImage.png to directory
                    out = socket.getOutputStream();
                    String line = request.headers.get(0);

                    // GET dir and all its contents
                    if (line.startsWith("GET / ")) {
                        // get contents as text
                        handleGetDirRequest(out);
                        Logger.debug("Directory " + topDir.getName() + " found");
                    }

                    // Get file from dir
                    else if (line.startsWith("GET /")){
                        // get contents of file
                        // parse out the file name
                        String fileName = parseFileName(line.substring(5));

                        // if there is no file to find
                        if (fileName != null){
                            File f = new File(topDir, fileName);
                            if (f.exists()){
                                handleGetFileRequest(f, out);
                                Logger.debug("File " + fileName + " found in Directory "
                                        + topDir.getName());
                            // file does not exist in in topDir
                            }else{
                                // logging error
                                Logger.error("File " + fileName +
                                        " does not exist in directory " + topDir.getName());
                            }
                        // there is no file to find
                        }else{
                            // logging error
                            Logger.error("No file to find in directory " + topDir.getName());
                        }
                    }

                    // POST file to dir
                    else if ( line.startsWith("POST /")){
                        // save contents of file
                        // parse out file name
                        String fileName = parseFileName(line.substring(6));

                        // if there is not a file name to POST
                        if (fileName != null){
                            // create new file
                            File f = new File(topDir, fileName);
                            // if there is already a file with 'fileName' that exists in directory
                            // if there is not the same file with 'fileName'
                            // is then added into the directory topDir
                            if (f.createNewFile()){
                                // if there is not any data
                                if(request.data != null){
                                    handlePostFileRequest(f, request.data);
                                    Logger.debug("File " + fileName + " added to directory "
                                            + topDir.getName() + "\n");
                                // data is not present
                                }else{
                                    // error
                                    Logger.error("Data is not present in " + request);
                                }
                            // file alread exists
                            }else{
                                // error
                                Logger.error("Already contains file " + fileName + " in dirctory "
                                        + topDir.getName());
                            }
                        // there is no file to add
                        }else{
                            // error
                            Logger.error("No file to add to directory "
                                    + topDir.getName());
                        }
                    }
                } catch (IOException inner) {
                    // cleanup and return
                    // logging error
                    Logger.error("IOException: ", inner);
                } finally {
                    // make sure all streams are closed
                    // input stream
                    if (in != null) {
                        // close in
                        try {
                            in.close();
                        } catch (IOException ee) {
                            // logging warning
                            Logger.warn("Input stream did not close properly ", ee);
                        }
                    }
                    // output stream
                    if (out != null) {
                        // close out
                        try {
                            out.close();
                        } catch (IOException ee) {
                            // logging warning
                            Logger.warn("Output stream did not close properly ", ee);
                        }
                    }
                    // make sure socket is closed
                    socket.close();
                }
            }
        }
    }

    /**
     *  Parses the inputstream into the HTTP headers
     *  and parses the raw data in 3 steps. HTTP headers
     *  and raw data are seperated with "\r\n\r\n".
     *  STEP 3 IS ONLY NEEDED FOR POST REQUESTS.
     *
     *  Steps:
     *      1. Read chunks of the input stream and write the
     *         chunks into byte array until all data has been grabed.
     *      2. Parse the byte array into string array. By converting
     *         into a solid string, then tokenzing the string with
     *         delim of "\r\n" into a string array.
     *      3. This step is only use for post.
     *         Once the headers have been parsed from the byte array,
     *         the raw data is then parsed from the ending position of
     *         the delim "\r\n\r\n". From that position to the end of
     *         byte array, that sub array is copied into another array.
     *
     *  Once all steps are completed the object Content is then returned
     *  with the headers in a String array and the data in a raw byte array.
     *
     *  @param in                        The input stream created from socket
     *  @return SocketHandler.Content    - The HTTP headers and raw data
     *  @throws IOException              Input stream my not have data
     */
    public Content readRequest(InputStream in) throws IOException {

        // initialization
        Content content = new Content();
        int SIZE = 1024;
        int c;
        byte[] chunk = new byte[SIZE];
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        // Step 1.
        // reading chunks of the inputstrem and written into
        // a byte array,
        while ((c = in.read(chunk)) != -1) {
            buffer.write(chunk, 0, c);
            buffer.flush();

            if (c < SIZE) {
                // nothing more to read
                break;
            }
        }

        // Step 2.
        // possible this is a POST, meaning there will be data at the end of the headers
        // both GET and POST will have a delimeter at the end of "2 blank lines" (\r\n\r\n)
        byte[] message = buffer.toByteArray();
        String wholeMesg = new String(message);
        int dataDelim = wholeMesg.indexOf("\r\n\r\n");

        // tokenizing byte array for headers
        String lines = new String(message, 0, dataDelim);
        StringTokenizer st = new StringTokenizer(lines, "\r\n");

        while (st.hasMoreTokens()){
            // adding header lines to string array
            content.headers.add(st.nextToken());
        }

        // Step 3.
        // rest of message[dataDelim + 4] is the data portion of the message
        // could be at the end of the array if this was a GET

        // grab data portion if it is present
        dataDelim += 4;
        // if there is any data present after "\r\n\r\n"
        if (dataDelim < message.length){
            //rest of  data to parse
            // c - dataDelim is the length of the data portion
            content.data = new byte[c - dataDelim];
            System.arraycopy(message, dataDelim, content.data, 0,c - dataDelim);
        }

        // clean up
        buffer.close();
        // return array with lines and with or without data portion
        return content;

    }

    /**
     *  Handles request for GET for whole directory.
     *  Scans through each item in directory one by one.
     *
     *  @param out      The output stream from socket
     */
    public void handleGetDirRequest(OutputStream out){
        // gets the list of files in topDir
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));

        String[] list  = topDir.list();
        // checks if directory contains items
        if (list != null){
            int i = 0;
            // displays all items in directory
            while (i < list.length){
                // writes each item in its own line
                writer.println(list[i]);
                i ++ ;
            }
        }

        // clean up
        writer.flush();
        // writer.close();      this will kill the socket, leaving it open
    }

    /**
     *  Handles request for GET file in directory.
     *  Grabs the data from the file in chunks and prints out the data.
     *
     *  @param file             The file in topDir and file path
     *  @param out              The output stream from socket
     *  @throws IOException     File may not exist
     */
    public void handleGetFileRequest (File file, OutputStream out) throws IOException{

        // initialization of input stream and creating byte array
        byte[] chunk = new byte[1042];
        FileInputStream in = null;

        try{

            int x = 0;
            in = new FileInputStream(file);

            while ((x = in.read(chunk)) != -1){
                out.write(chunk,0,x);
            }
            out.flush();

        } finally {
            if (in != null){
                in.close();
            }
        }
    }

    /**
     *  Handles POST for a file to directory.
     *  Writes 'data' into 'file'.
     *
     *  @param file             The file in topDir and file path
     *  @param data             The raw data to be added into file
     *  @throws IOException     File may not exist
     */
    public void handlePostFileRequest (File file, byte[] data) throws IOException{

        // initializes output stream
        FileOutputStream out = null;

        try {

            out = new FileOutputStream(file);
            // writes byte[] to file
            out.write(data);

            // clean up
            out.flush();
            out.close();
            out = null;

        } finally {
            if (out != null){
                // final clean up and logs warning
                out.close();
                Logger.warn("Output stream was not closed properly");
            }
        }
    }

    /**
     *  Parses string input to find the file name.
     *  By finding substring before " HTTP".
     *
     *  @param in           Line from HTTP request containing file name
     *  @return String      - Parsed string containing the file name
     */
    public String parseFileName(String in){
        // finds start position of " HTTP" in 'in'
        int offset = in.indexOf(" HTTP");
        if(offset != -1){
            // returns substring before " HTTP"
            return in.substring(0,offset);
        }
        // logs error and returns nothing
        Logger.error("Problem parsing string: " + in);
        return null;
    }

    /**
     *  Class to store request object.
     *  Contains String list of headers
     *  and raw data after headers.
     */
    public class Content {
        protected ArrayList<String> headers = new ArrayList<String>();
        protected byte[] data;
    }
}
