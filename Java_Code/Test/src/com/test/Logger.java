package com.test;

import java.io.IOException;

/**
 *  Logger
 *
 *  Handles all logging: can handle debugging logs, warning logs,
 *  and error logs. Each method takes a String message that is custom
 *  for each debug, warning, or error. Prints the message with a
 *  corresponding header in the terminal. Able to print stack trace's
 *  about IOExceptions for warnings and errors.
 */
public class Logger {

    /**
     *  Prints out the specific message in terminal.
     *
     *  @param message      A specific debugging message
     */
    public static void debug(String message){
        System.out.println(message);
    }

    /**
     *  Prints out "WARNING" and the message about the warning.
     *
     *  @param message      A specific message about a warning
     */
    public static void warn(String message){
        System.err.println("WARNING " + message);
    }

    /**
     *  Prints out "WARNING", the message about the warning, and the
     *  warning itself. Prints stack trace of IOException.
     *
     *  @param message      A specific message about a warning
     *  @param exception    The IOException that caused the warning
     */
    public static void warn(String message, IOException exception){
        System.err.println("WARNING " + message + " " + exception);
        exception.printStackTrace();
    }

    /**
     *  Prints out "Error" and the message about the error.
     *
     *  @param message      A specific message about a error
     */
    public static void error(String message){
        System.err.println("ERROR " + message);
    }

    /**
     *  Prints out "ERROR", the message about error, and the error
     *  itself. Prints stack trace of IOException.
     *
     *  @param message      A specific message about a error
     *  @param exception    The IOException that caused the error
     */
    public static void error(String message, IOException exception){
        System.err.println("ERROR " + message + " " + exception);
        exception.printStackTrace();
    }
}
