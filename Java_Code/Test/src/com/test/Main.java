package com.test;

import java.io.File;
import java.io.IOException;

/**
 *  Main
 *
 *  Runs the server, takes inputs from args. If no args are present
 *  then defult args are used to run the server. Calls on SocketHandler to
 *  connect client to server.
 */
public class Main {

    // variables
    private static final int DEFAULT_PORTNUM = 8080;
    private static final String DEFAULT_DIR = "/Users/Johnny/testdir";

    /**
     *  Starts the server by calling on SocketHandler.
     *  Args should be portNumber and then root directory.
     *  Should look something like:
     *      8080 "/Users/name/...."
     *
     *  @param args     A string of arguments, should only have atmost 2 arguments
     */
    public static void main(String[] args) {
        // initialization
        int portNumber = DEFAULT_PORTNUM;
        String dir = DEFAULT_DIR;

        // if there is at least 1 arguments
        if (args.length >= 1){
            // users supplies the port number
            portNumber = Integer.parseInt(args[0]);
            // if there is at least 2 arguments
            if (args.length >= 2){
                // user supplies dir
                dir = args[1];
            }
        }

        try {
            // create instance of SocketHandler
            SocketHandler helper = new SocketHandler();
            // start the socket with the port number and root dir
            helper.start(portNumber, new File(dir));

        } catch(IOException foo) {
            // logging error
            Logger.error("Server failed ", foo);
        }
    }
}
