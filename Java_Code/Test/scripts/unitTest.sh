#!/usr/bin/env bash

# run unit tests
# should be run from topdir. For example:
#    scripts/unitTest.sh

# start the server
java -cp out/production/Test/ com.test.Main 8080 "/Users/Johnny/testdir" &
sleep 3

# use curl to test get and post
echo "Unit: Getting directory contents"
curl http://localhost:8080/

echo "Unit: Sending text file"
curl --data-ascii 'abcdefhijklmnopqrstuvwxyz' http://localhost:8080/test.txt

echo "Unit: Getting new text file"
curl http://localhost:8080/test1.txt

# kill background process
pkill java
