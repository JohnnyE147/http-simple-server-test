#!/usr/bin/env bash

# run javadoc utility and put output into docs subdir
# should be run from topdir. For example:
#    scripts/docs.sh

rm -rf docs
mkdir docs
javadoc -d docs -sourcepath src com.test