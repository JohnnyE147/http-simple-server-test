# Simple HTTP Server Socket

This is just an example simple http server with sockets in C and in Java. 
Both programs can do 3 different types of request from the client.
The following request a client can ask are:

*  A GET request that will return back a texted-based list of a directory.
*  A GET request that will return back a PNG file from a directory.
*  A POST request that will place a PNG file into a directory.

For the second GET the client can also ask for any type of file to find.

---

## Prerequisites

If you wish to run these programs inside of a IDE.

The C program was made inside Xcode which caqn be downloaded from Apple, if you have a Mac.
If you do not use a Mac then any IDE you chose will be acceptable. You just have to import the C source files into a new C project.

The Java program was made inside IntelliJ IDEA CE, which is free to download from their website.

You do not need to use these IDEs to run these programs, it's simply what I used. 

---

## Running Tests

Both Java and C programs have a shell script to run some simple unit test.
These scripts should be run from their respected directories. 
For example:	
```
Scripts/unitTest.sh
```
	
Will start the program in either C or Java and perform 3 simple test cases.

In order to run the C program a build file must be created.
From there the execturable can then run the program without needing a IDE.
It should look something like this in command line:
```
../Build/Test 8080 "~/someDir" &
```

In order to run the Java program .class files must be created for the 3 java files.
Once those are created it can be run as a java program under com.test.Main
It should look something like this in command line:
```
java -cp ../Test/ com.test.Main 8080 "~/someDir" &
```
	
Both prgrams take 2 arguments the first is the port number which is just an integer.
The second is a string that is the path to any directory you choose.
If you do not decide to use these arguments then there are default values that can be change inside the programs in their Main.
	